let clientesObtenidos;

function getClientes() {
    let url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers"
    let request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if(this.readyState == 4 && this.status == 200){
            clientesObtenidos = request.responseText;

            perocesarClientes();   
        }
    }

    request.open("GET", url, true);
    request.send();
}

function perocesarClientes() {
    let rutaBandera = 'https://www.countries-ofthe-world.com/flags-normal/flag-of-'
    let jsonClientes = JSON.parse(clientesObtenidos)

    let divTabla = document.getElementById("divTablaClientes");
    let tabla = document.createElement("table");
    let tBody =  document.createElement("tBody");

    tabla.classList.add("table")
    tabla.classList.add("table-striped")

    jsonClientes.value.forEach((element, idx) => {
       let nuevaFila = document.createElement("tr");
       let columnaNombre =  document.createElement("td");
       let columnaCiudad =  document.createElement("td");
       let columnaBandera =  document.createElement("td");
       let imgBandera = document.createElement("img");

       if(element.Country == "UK"){
        imgBandera.src = `${rutaBandera}United-Kingdom.png`;
       } else{
        imgBandera.src = `${rutaBandera}${element.Country}.png`;
       }
       
       
       imgBandera.classList.add('flag');

       columnaNombre.innerText = element.ContactName;
       columnaCiudad.innerText = element.City;
       columnaBandera.appendChild(imgBandera);

       nuevaFila.appendChild(columnaNombre);
       nuevaFila.appendChild(columnaCiudad);
       nuevaFila.appendChild(columnaBandera);

       tBody.appendChild(nuevaFila);
    });

    tabla.appendChild(tBody);
    divTabla.appendChild(tabla);
}
