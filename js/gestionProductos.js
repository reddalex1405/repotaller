let productosObtenidos;

function getProductos() {
    let url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products"
    let request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if(this.readyState == 4 && this.status == 200){
            //console.table(JSON.parse(request.responseText).value);
            productosObtenidos = request.responseText;

            perocesarProductos();   
        }
    }

    request.open("GET", url, true);
    request.send();
}

function perocesarProductos() {
    let jsonProductos = JSON.parse(productosObtenidos)

    let divTabla = document.getElementById("divTabla");
    let tabla = document.createElement("table");
    let tBody =  document.createElement("tBody");

    tabla.classList.add("table")
    tabla.classList.add("table-striped")

    jsonProductos.value.forEach((element, idx) => {
       let nuevaFila = document.createElement("tr");
       let columnaNombre =  document.createElement("td");
       let columnaPrecio =  document.createElement("td");
       let columnaStock =  document.createElement("td");

       columnaNombre.innerText = element.ProductName;
       columnaPrecio.innerText = element.UnitPrice;
       columnaStock.innerText = element.UnitsInStock;

       nuevaFila.appendChild(columnaNombre);
       nuevaFila.appendChild(columnaPrecio);
       nuevaFila.appendChild(columnaStock);

       tBody.appendChild(nuevaFila);
    });

    tabla.appendChild(tBody);
    divTabla.appendChild(tabla);
    
  //  alert(jsonProductos.value[0].ProductName)
}
